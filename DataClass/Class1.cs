﻿using System;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;

namespace DataClass
{
    public class DataStructureClass
    {

        #region Структуры для получения Id сканнера по ip станции
        //станция нам отправляет: свой ip
        public struct structScannerIdClient
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string stationIp;
        }


        //мы ей возвращаем id сканнера, который отсканировал нтрихкод с ее Id
        public struct structScannerIdServer
        {
            public bool result;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string id;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string exception;
        }


        public struct structScannerValueClient
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string stationId;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string scannerId;
        }


        public struct structScannerValueServer
        {
            public bool result;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string value;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string stationId;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string scannerId;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string exception;
        }
        #endregion

        #region AGV
        //AGV
        public struct structAgvOnStationClient
        {
            public string stationId;
        }
        public struct structAgvOnStationServer
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string stationId;
            public bool agvOnStation;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string sendException;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string seqId;
        }
        #endregion

        #region SCAN
        //SCAN
        public struct structScanStationIdClient
        {
            public string stationIp;
            public int stationType;
        }
        public struct structScanStationIdServer
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string stationIp;
            public int stationType;
            public bool scanResult;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string scanId;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string exception;
        }
        #endregion

        //ИНФОРМАЦИЯ ПО СТАНЦИИ
        public struct structStationInfo
        {
            public string name;
            public string ip;
            public string id;
            public string index;
            public string scannerId;
            public int type;
            public int timeoutServerAnswer;
            public string queueNameServer;
            public string queueNameScanner;
            public string queueNameAlive;
            public string hostAddress;
            public string boxIp;
            public int state;
            public bool serverState;
        }

        #region АВТОРИЗАЦИЯ ПОЛЬЗОВАТЕЛЯ
        //USER
        public struct structUserAuthClient
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string login;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string password;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string stationId;

        }

        public struct structUserAuthServer
        {
            public bool authResult;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string userId;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string userLogId;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string userName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string stationId;
            public int printerId;
            public int scannerId;
            public int shtrihId;
            public int tsdId;
            public int permissionAssembly11;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string authException;
        }

        #endregion

        #region АВТОРИЗАЦИЯ МАСТЕРА
        public struct structMasterAuthClient
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string login;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string password;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string stationId;
            public structMasterAuthClient(
                        string login,
                        string password,
                        string stationId)
            {
                this.login = login;
                this.password = password;
                this.stationId = stationId;
            }
        }

        public struct structMasterAuthServer
        {
            public bool authResult;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string masterName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string masterId;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string[] reasons;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string stationId;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string printerId;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string scannerId;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string authException;
        }
        #endregion


        #region ПРИЧИНЫ ДЛЯ МАСТЕРА
        //причины вызова мастера
        public struct structMasterReasonsServer
        {
            public bool result;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string masterId;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string stationId;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string exception;
        }

        public struct structMasterReasonsClient
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string masteId;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string stationId;
            public int reasonNumber;

        }
        #endregion

        //кодирование пароля
        public class Coding
        {
            public static string calculateMD5Hash(string input)
            {
                // step 1, calculate MD5 hash from input
                MD5 md5 = System.Security.Cryptography.MD5.Create();
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hash = md5.ComputeHash(inputBytes);
                // step 2, convert byte array to hex string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hash.Length; i++)
                {
                    sb.Append(hash[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

        //проверка поля ввода
        public class FieldChecking
        {
            /// <summary>
            /// Является ли строка null, пустой или содержит пробелы
            /// </summary>
            /// <param name="input">строка для проверки</param>
            /// <returns></returns>
            public static bool fieldCheckingInput(params string[] input)
            {
                bool result = true;
                foreach (var item in input)
                {
                    if (item.Length < 4 || String.IsNullOrEmpty(item) || item.IndexOf(' ') != -1)
                    {
                        result = false;
                    }
                }
                return result;
            }
        }


        #region авторизация станции
        //
        public struct structStationAuthClient
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string ip;
            public int type;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string phraseSecret;
        }

        //ответ от сервера при авторизации станции
        public struct structStationAuthServer
        {
            public bool result;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string id;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string name;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string index;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string ip;
            public int type;//1 сборка 2 подсб 3 тест 4 ремонт
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string boxIp;
            public int state;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string exception;
        }
        #endregion

        //шаг для seq
        public struct structProductSteps
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string seqId;
            public int stepNumber;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string text;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string picture;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string video;
            public int triggerComplete;
            public int timeLimit;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string triggerString1;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string triggerString2;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string triggerError;
        }

        //SEQ
        public struct structSEQ
        {
            public bool nowSeq;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string seqId;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string productionName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string productionIndex;
            public int seqOnStationState;
            public int completePercent;
            public int stepNow;
            public int timeWaitLimit;
            public int timeWorkLimit;
            public structProductSteps[] seqSteps;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string timeWorkStart;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string timeWorkFinish;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string exception;
        }



        #region Структуры для проверки состояния Серверов(201)

        public struct structStationAliveClient
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string id;
        }
        public struct structStationAliveServer
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string exception;
            public bool result;
        }
        #endregion


        #region Структуры для установки следующего состояния seq на станции (191)
        //станция нам отправляет: seqId и stationId
        public struct structSeqNextStateClient
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string seqId;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string stationId;
        }
        public struct structSeqNextStateServer
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string exception;
            public bool result;
            public int state;
        }
        #endregion


        #region Структуры для установки  следующего значения шага seq (190)
        //станция нам отправляет: seqId и stationId
        public struct structSeqOnStationStepClient
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string seqId;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string stationId;
        }

        public struct structSeqOnStationStepServer
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string exception;
            public bool result;
            public int stepNow;
        }

        #endregion


        #region ПОлУЧЕНИЕ ВСЕХ SEQ ЗА ПРОШЛУЮ И ТЕКУЩЮ СМЕНЫ (ЕСЛИ НЕТ ТЕКУЩЕЙ ПОЛУЧИМ ТОЛЬКО ЗА ПРОШЛУЮ)

        public struct structSeqShiftOldAndNowServer
        {
            public bool result;
            public int[] shifts;
            public structSEQ[] seqOnStationShifts;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string exception;
        }


        public struct structSeqShiftOldAndNowServer1
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 10000)]
            public string lastJson;
        }

        public struct structSeqShiftOldAndNowClient
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string stationId;
        }
        #endregion





        #region получение даты и номера текущей смены
        public struct structShiftNowGetServer
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string date;
            public int number;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string exception;
        }
        #endregion

        #region Выход пользователя //240
        public struct structUserExitOnStationClient
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string stationId;
        }

        public struct structUserExitOnStationServer
        {
            public bool result;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string exception;
        }
        #endregion

        #region вызов мастера
        //210 вызван пользователем
        //211 время ожидания
        //212 время работы
        //213 время шага
        //214 прочее
        public struct structCallMasterClient
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string stationId;
            public int reasons;
        }

        public struct structCallMasterServer
        {
            public bool result;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string exception;
        }
        #endregion
    }

    public class MongoDatabaseClass
    {

    }

    public class SQLDatabaseClass
    {

    }

    public class RPCManagamentClass
    {

    }
}
